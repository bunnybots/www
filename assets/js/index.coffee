$ = (element) ->
  elements = document.querySelectorAll(element)
  
  if elements.length is 1
    elements[0]
  else
    elements

print = console.log

clock_element = $('#clock')
timeline = {}
timeline.outer = $('#timeline')
timeline.completed = $('#timeline > #completed')

clock = {}

units = ['days', 'hours', 'minutes', 'seconds']

for unit in units
  new_element = document.createElement('div')
  clock[unit] = clock_element.appendChild(new_element)
  clock[unit].classList.add(unit)

time_until = (date, startTime = new Date) ->
  time = Date.parse(date) - Date.parse(startTime)
  seconds = Math.floor(time / 1000 % 60)
  minutes = Math.floor(time / 1000 / 60 % 60)
  hours = Math.floor(time / (1000 * 60 * 60) % 24)
  days = Math.floor(time / (1000 * 60 * 60 * 24))
  {
    'total': time
    'days': days
    'hours': hours
    'minutes': minutes
    'seconds': seconds
  }

total_duration = time_until('2016-12-30 8:00', '2016-09-01 8:00').total

update_time = ->
  time_left = time_until '2016-12-30 8:00'
  for unit in units
    if time_left[unit] is 1
      human_unit = unit.slice(0, -1)
    else
      human_unit = unit

    if human_unit < 0
      human_unit = 0


    if time_left[unit] isnt 0
      clock[unit].innerHTML = "#{time_left[unit]} #{human_unit}"
    else
      clock[unit].innerHTML = ''

  timeline.completed.style.width = (100 - (time_left.total / total_duration) * 100).toFixed(4) + '%'


update_time()

setInterval(update_time, 1000)

scrolling = false

window.addEventListener 'scroll', ->
  scrolling = true

setInterval (->
  if scrolling
    scrolling = false
    on_scroll()
), 15

clamp = (value, min, max) ->
  if value < min
    return min
  else if value > max
    return max
  else
    return value

on_scroll = ->
  scroll_amount = document.body.scrollTop

  clock_element.style.opacity = 1 - scroll_amount / 200

  red = scroll_amount / 2
  green = scroll_amount / 2
  blue = scroll_amount / 2

  #         r   g    b
  start = [33, 33,  33]
  end   = [46, 50, 147]

  red   = clamp(red,   start[0], end[0])
  green = clamp(green, start[1], end[1])
  blue  = clamp(blue,  start[2], end[2])

  document.body.style.backgroundColor = "rgb(#{red}, #{green}, #{blue})"

on_scroll()

